
public class LinkedList 
{
	Node first = null;
	Node last = null;
	private int size = 0; // Keeps track of LL length
	
	public LinkedList()
	{
		clear();
	}
	
	public Boolean isEmpty()
	{
		if(first == null)
		{
			return true;
		}
		else
		{
			return false;
		}
			
	}
	
	public void addFirst(String d) // adds new data to front of LL
	{
		Node n = new Node(d, first);
		first = n;
		size++;
		if(last == null)
		{
			last = first;
		}
	}

	public void remFirst()
	{
		if(last == first)
		{
			last = null;
		}
		first = first.next;
		size--;
	}
	
	public int size()
	{
		return size;
	}
	
	public void clear() // removes reference to rest of LL 
	{
		first = null;
		last = null;
		size = 0;
	}

	public String printNode(int n) // takes in an int n for location of node, then prints it
	{	
		Node m = new Node(first.data, first.next);
		int counter = 0;
		if(n == 0)
		{
			return first.data;
		}
		else if(n == size-1)
		{
			return last.data;
		}
		else if(n >= size || n < 0)
		{
			return "Node not found.";
		}
		else
		{
			while(counter != n)
			{
				m.data = m.next.data;
				m.next = m.next.next;
				counter++;
			}
			return m.data;
		}
	}
		
	public void printLL() // test method to print out linked list
	{
		for(int i = 0; i < size; i++)
		{
			System.out.println(printNode(i));
		}
	}
}
