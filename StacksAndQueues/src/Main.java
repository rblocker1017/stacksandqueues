/**
 * Created by Ryan on 2/1/2019.
 */
public class Main
{
    public static void main(String[] args)
    {
    	String[] testArr = new String[]{"a" , "b" , "c"};
    	
    	// MyStack testing
    	
    	MyStack ms = new MyStack(); // creates an empty stack
    	System.out.println("printStack printing an empty stack. Expects nothing:");
    	ms.printStack(); // expects nothing
    	System.out.print("isEmpty checking an empty stack. Expects true: ");
    	System.out.println(ms.isEmpty()); // expects true

    	ms.push("a");
    	System.out.print("printStick printing a stack with only the value 'a'. Expects a: ");
    	ms.printStack(); // expects "a"
    	
    	ms.pop();
    	System.out.println();
    	System.out.println("popped off a. printStack now should print nothing:");
    	ms.printStack(); // expects nothing
    	
    	ms.push("b");
    	ms.push("c");
    	System.out.print("pushed values b and c to stack. printStack should print 'c' then 'b': ");
    	ms.printStack(); // expects c, b
    	
    	
    	MyStack ms1 = new MyStack(testArr);
    	System.out.println();
    	System.out.print("created new stack with values 'a', 'b', 'c'. printStack should print 'c', 'b', 'a': ");
    	ms1.printStack(); // expects c, b, a
    	
    	System.out.println();
    	System.out.print("isEmpty on a stack with values. Expects false: ");
    	System.out.println(ms.isEmpty()); // expects false
    	
    	ms1.pop();
    	System.out.print("Popped off stack. Expects 'b', 'a': ");
    	ms1.printStack();
    	
    	System.out.println();
    	ms1.pop(); // remove b 
    	ms1.pop(); // remove a
    	System.out.print("Popped a third time. Popping from an empty stack should return an error message: ");
    	ms1.pop(); // try to remove nothing

    	
    	
    	System.out.println();
    	System.out.println("----");
    	System.out.println();
    	
    	// MyQueue testing

    	MyQueue mq = new MyQueue();
    	System.out.println("printQueue printing an empty queue. Expects nothing:");
    	mq.printQueue(); // expects nothing
    	System.out.print("isEmpty checking an empty queue. Expects true: ");
    	System.out.println(mq.isEmpty()); // expects true
    	
    	mq.enqueue("hello");
    	System.out.print("printQueue printing a queue with only the value 'hello'. Expects hello: ");
    	mq.printQueue(); // expects "hello"
    	mq.enqueue("world");
    	System.out.println();
    	System.out.print("printQueue printing a queue with only the value 'hello' then 'world'. Expects hello world: ");
    	mq.printQueue(); // expects "hello world"
    	mq.dequeue();
    	System.out.println();
    	System.out.print("printQueue printing a queue with only the value of 'world' after using dequeue. Expects world: ");
    	mq.printQueue();
    	System.out.println();
    	System.out.print("Dequeueing an empty queue. Expects an error message: ");
    	mq.dequeue();
    	mq.dequeue();
    	
    	MyQueue mq1 = new MyQueue(testArr);
    	System.out.println();
    	System.out.print("Printing queue with values 'a', 'b', 'c'. Expects a, b, c: ");
    	mq1.printQueue();
    }

}
