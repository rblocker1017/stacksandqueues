
public class MyStack 
{
	LinkedList ll = new LinkedList();
	public MyStack() // default constructor
	{
	}
	
	public MyStack(String[] list) // creates a stack depending on what is passed in
	{
		for(int i = 0; i < list.length; i++)
		{
			ll.addFirst(list[i]);
		}
	}
	
	public String pop() // retrieves and deletes data on top of stack
	{
		if(ll.isEmpty())
		{
			System.out.print("No values to remove.");
			return null;
		}
		String rv = ll.printNode(0);
		ll.remFirst();
		return rv;
	}
	public void push(String data) // adds data on top of stack
	{
		ll.addFirst(data);
	}
	public Boolean isEmpty() // checks for values in stack
	{
		return ll.isEmpty();
	}
	
	public String[] printStack2() // prints value of stack to console using size method and ll.printNode method
	{
		String[] arr = new String[ll.size()];
		for(int i = 0; i < ll.size(); i++ )
		{
			System.out.print(ll.printNode(i) + " ");
			arr[i] = ll.printNode(i); 
		}
		return arr;
	}
	
	public void printStack() // prints values of stack to console without size method 
	{
		if(ll.isEmpty())
		{
			// do nothing
		}
		else
		{
			Node n = new Node(ll.first.data, ll.first.next);
			while(n.next != null)
			{
				System.out.print(n.data + " ");
				n = n.next;
			}
			System.out.print(n.data);
		}
	}

}
