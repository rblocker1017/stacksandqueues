
public class MyQueue 
{
	MyStack ms = new MyStack();
	MyStack ms2 = new MyStack();
	public MyQueue()
	{
		
	}
	public MyQueue(String[] list)
	{
		for(int i = 0; i < list.length; i++)
		{
			ms.push(list[i]);
		}
		
		for(int i = 0; i < list.length; i++)
		{
			ms2.push(ms.pop());
		}
	}
	
	public void enqueue(String data)
	{
		MyStack temps = new MyStack();
		
		while(!ms2.isEmpty())
		{
			temps.push(ms2.pop());
		}
		ms2.push(data);
		while(!temps.isEmpty())
		{
			ms2.push(temps.pop());
		}
	}
	
	public String dequeue()
	{
		return ms2.pop();
	}
	
	public Boolean isEmpty()
	{
		return ms2.isEmpty();
	}
	
	public void printQueue()
	{
		ms2.printStack();
	}

	
}
